package com.example.prabin.old_is_gold_v2.UserLoginAndRegistration;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.prabin.old_is_gold_v2.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthEmailException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;

public class SignupActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    private EditText mEditEmail, mEditPassword;
    private Button mEditSignup;

    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        mAuth = FirebaseAuth.getInstance();

        mEditEmail = (EditText) findViewById(R.id.signup_editEmail);
        mEditPassword = (EditText) findViewById(R.id.signup_editPassword);

        mEditSignup = (Button) findViewById(R.id.signup_btnSignup);

        mProgressBar = (ProgressBar) findViewById(R.id.signup_ProgressBar);
        mProgressBar.setVisibility(View.GONE);

        mEditPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    attemptSignup();
                    return true;
                }
                return false;
            }
        });

        mEditSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptSignup();
            }
        });
    }

    private void attemptSignup() {

        //hide keyboard
        View v = this.getCurrentFocus();
        if (v != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }

        mEditSignup.setEnabled(false);
        mProgressBar.setVisibility(View.VISIBLE);

        final String email = mEditEmail.getText().toString();
        String password = mEditPassword.getText().toString();

        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            mEditSignup.setEnabled(true);
            mProgressBar.setVisibility(View.INVISIBLE);
            return;
        }

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {
                            sendVerificationEmail();
                        } else {
                            Toast.makeText(SignupActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();

                            mEditSignup.setEnabled(true);
                            mProgressBar.setVisibility(View.INVISIBLE);
                        }
                    }
                });
    }

    private void sendVerificationEmail() {

        FirebaseUser user = mAuth.getCurrentUser();
        if (user == null) {
            return;
        }
        user.sendEmailVerification()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        if (task.isSuccessful()) {
                            showConfirmationDialog();
                        } else {
                            Toast.makeText(SignupActivity.this, "Email Couldn't Be Sent! Try again", Toast.LENGTH_SHORT).show();
                        }

                        mEditSignup.setEnabled(true);
                        mProgressBar.setVisibility(View.VISIBLE);
                    }

                    private void showConfirmationDialog() {
                        AlertDialog.Builder builder = new AlertDialog.Builder(SignupActivity.this);
                        builder.setTitle("Sign up successful");
                        builder.setMessage("Verification Email Sent!\nPlease verify your email first before logging in.");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        });
                        builder.show();
                    }
                });

    }
}
